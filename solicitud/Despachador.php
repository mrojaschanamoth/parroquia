<?php
namespace solicitud;
use controladores;

class Despachador{
  private $solicitud;

  public function __construct(){
    $this->solicitud = Router::getSolicitud();
  }

  public function despachar()
  {
    $controlador = $this->cargarControlador();
    if(count($_SESSION)>0){
      if(method_exists($controlador,$this->solicitud->accion)){
        $controlador->{$this->solicitud->accion}();
      }else{
        $controlador = new PlantillaControlador();
        $controlador->vista404();
      }
    }else{
      $controlador = new controladores\UsuarioControlador();
      $controlador->login();
    }
  }

  private function cargarControlador()
  {
    $Controlador = 'controladores\\'.ucwords(strtolower($this->solicitud->controlador)).'Controlador';
    if(class_exists($Controlador))
    {
      return new $Controlador();
    }else{
      $this->solicitud->accion = 'vista404';

    }
  }
}
