<?php 
  namespace modelos;

  class ParrocoModelo extends Modelo{

    public function __construct(){
      parent::__construct('SP_PARROCO_CRUD(?,?,?,?,?)','Parroco');
    }

    public function listarAll(){
      $objeto=new $this->claseEntidad;
      $objeto->opcion=6;
      $respuesta=$this->queryObjects($this->sp,$objeto);
      return $respuesta;
    }
  }