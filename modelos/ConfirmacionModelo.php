<?php 
  namespace modelos;

  class ConfirmacionModelo extends Modelo{

    public function __construct(){
      parent::__construct('SP_CONFIRMACION_CRUD(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)','Confirmacion');
    }
    public function listarAll(){
      $objeto=new $this->claseEntidad;
      $objeto->opcion=4;
      $respuesta=$this->queryObjects($this->sp,$objeto);
      return $respuesta;
    }
  }