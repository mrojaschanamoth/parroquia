<?php 
  namespace modelos;

  class BautismoModelo extends Modelo{

    public function __construct(){
      parent::__construct('SP_BAUTISMO_CRUD(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)','Bautismo');
    }
    public function listarAll(){
      $objeto=new $this->claseEntidad;
      $objeto->opcion=4;
      $respuesta=$this->queryObjects($this->sp,$objeto);
      return $respuesta;
    }
  }