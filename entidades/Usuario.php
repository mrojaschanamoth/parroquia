<?php

  namespace entidades;
  use PDO;

  class Usuario extends Entidad{
    public $codigo;
    public $user;
    public $clave;
    public $nombre;
    public $apPaterno;
    public $apMaterno;
    public $direccion;

    public function setConsulta($filaConsulta)
    {
      $this->codigo = $this->obtenerColumna($filaConsulta, 'IdUsuario');
      $this->user = $this->obtenerColumna($filaConsulta,'Usuario');
      $this->clave = $this->obtenerColumna($filaConsulta,'Clave');
      $this->nombre = $this->obtenerColumna($filaConsulta,'Nombre');
      $this->apPaterno = $this->obtenerColumna($filaConsulta, 'ApellidoPaterno');
      $this->apMaterno = $this->obtenerColumna($filaConsulta, 'ApellidoMaterno');
      $this->direccion = $this->obtenerColumna($filaConsulta,'Direccion');
    }

    public function bindValues($statement)
    {
      $statement->bindValue(1,$this->codigo,PDO::PARAM_INT);
      $statement->bindValue(2,$this->user,PDO::PARAM_STR);
      $statement->bindValue(3,$this->clave,PDO::PARAM_STR);
      $statement->bindValue(4,$this->nombre,PDO::PARAM_STR);
      $statement->bindValue(5,$this->apPaterno,PDO::PARAM_STR);
      $statement->bindValue(6,$this->apMaterno,PDO::PARAM_STR);
      $statement->bindValue(7,$this->direccion,PDO::PARAM_STR);
      $statement->bindValue(8,$this->opcion,PDO::PARAM_INT);
		  $statement->bindValue(9,$this->pagina,PDO::PARAM_INT);
      return $statement;
    }

    public function set($metodo){
      $this->codigo = $metodo('IdUsuario');
      $this->user = $metodo('Usuario');
      $this->clave = $metodo('Clave');
      $this->nombre = $metodo('Nombre');
      $this->apPaterno = $metodo('apPaterno');
      $this->apMaterno = $metodo('apMaterno');
      $this->direccion = $metodo('direccion');
    }

    public function iniciarSesion(){
      $_SESSION['codigo'] = $this->codigo;
      $_SESSION['usuario'] = $this->user;
      $_SESSION['nombre'] = $this->nombre.' '.$this->apPaterno.' '.$this->apMaterno;
    }
  }