<?php

  namespace entidades;
  use PDO;

  class Parroquia extends Entidad{
    public $codigo;
    public $nombre;
    public $direccion; 
    public $cod_distrito;
    public $telefono;

    public function setConsulta($filaConsulta)
    {
      $this->codigo = $this->obtenerColumna($filaConsulta, 'IdParroquia');
      $this->nombre = $this->obtenerColumna($filaConsulta,'Nombre');
      $this->direccion = $this->obtenerColumna($filaConsulta, 'Direccion');
      $this->cod_distrito = $this->obtenerColumna($filaConsulta,'IdDistrito');
      $this->telefono = $this->obtenerColumna($filaConsulta,'Telefono');
    }

    public function bindValues($statement)
    {
      $statement->bindValue(1,$this->codigo,PDO::PARAM_STR);
      $statement->bindValue(2,$this->nombre,PDO::PARAM_STR);
      $statement->bindValue(3,$this->direccion,PDO::PARAM_STR);
      $statement->bindValue(4,$this->cod_distrito,PDO::PARAM_STR);
      $statement->bindValue(5,$this->telefono,PDO::PARAM_STR);
      $statement->bindValue(6,$this->opcion,PDO::PARAM_INT);
		  $statement->bindValue(7,$this->pagina,PDO::PARAM_INT);
      return $statement;
    }

    public function set($metodo){
      $this->codigo = $metodo('IdParroquia');
      $this->nombre = $metodo('Nombre');
      $this->direccion = $metodo('Direccion');
      $this->cod_distrito = $metodo('IdDistrito');
      $this->telefono = $metodo('Telefono');
    }
  }