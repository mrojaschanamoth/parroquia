<?php

  namespace entidades;
  use PDO;

  class Bautismo extends Entidad{
    public $codigo;
    public $cod_libro;
    public $cod_foja;
    public $cod_numero;
    public $cod_parroquia;
    public $padre_nombre;
    public $padre_apPaterno;
    public $padre_apMaterno;
    public $nombre;
    public $madre_nombre;
    public $madre_apPaterno;
    public $madre_apMaterno;
    public $nacimiento;
    public $fnacimiento;
    public $fbautismo;
    public $madrina_nombre;
    public $madrina_apPaterno;
    public $madrina_apMaterno;
    public $padrino_nombre;
    public $padrino_apPaterno;
    public $padrino_apMaterno;
    public $anotaciones;
    public $sexo;
    public $naturalmadre;
    public $naturalpadre;
    public $cod_distrito;
    public $telefono;

    public function setConsulta($filaConsulta)
    {
      $this->codigo = $this->obtenerColumna($filaConsulta,'IdBautismo');
      $this->cod_libro = $this->obtenerColumna($filaConsulta,'IdLibro');
      $this->cod_foja = $this->obtenerColumna($filaConsulta,'IdFoja');
      $this->cod_numero = $this->obtenerColumna($filaConsulta,'IdNumero');
      $this->cod_parroquia = $this->obtenerColumna($filaConsulta,'IdParroquia');
      $this->nombre = $this->obtenerColumna($filaConsulta,'Nombre');
      $this->padre_nombre = $this->obtenerColumna($filaConsulta,'PadreNombre');
      $this->padre_apPaterno = $this->obtenerColumna($filaConsulta,'PadreApellidoPaterno');
      $this->padre_apMaterno = $this->obtenerColumna($filaConsulta,'PadreApellidoMaterno');
      $this->madre_nombre = $this->obtenerColumna($filaConsulta,'MadreNombre');
      $this->madre_apPaterno = $this->obtenerColumna($filaConsulta,'MadreApellidoPaterno');
      $this->madre_apMaterno = $this->obtenerColumna($filaConsulta,'MadreApellidoMaterno');
      $this->nacimiento = $this->obtenerColumna($filaConsulta,'LugarNacimiento');
      $this->fnacimiento = $this->obtenerColumna($filaConsulta,'FechaNacimiento');
      $this->fbautismo = $this->obtenerColumna($filaConsulta,'FechaBautismo');
      $this->madrina_nombre = $this->obtenerColumna($filaConsulta,'MadrinaNombre');
      $this->madrina_apPaterno = $this->obtenerColumna($filaConsulta,'MadrinaApellidoPaterno');
      $this->madrina_apMaterno = $this->obtenerColumna($filaConsulta,'MadrinaApellidoMaterno');
      $this->padrino_nombre = $this->obtenerColumna($filaConsulta,'PadrinoNombre');
      $this->padrino_apPaterno = $this->obtenerColumna($filaConsulta,'PadrinoApellidoPaterno');
      $this->padrino_apMaterno = $this->obtenerColumna($filaConsulta,'PadrinoApellidoMaterno');
      $this->anotaciones = $this->obtenerColumna($filaConsulta,'Anotaciones');
      $this->sexo = $this->obtenerColumna($filaConsulta,'Sexo');
      $this->naturalmadre = $this->obtenerColumna($filaConsulta,'NaturalMadre');
      $this->naturalpadre = $this->obtenerColumna($filaConsulta,'NaturalPadre');
      $this->cod_distrito = $this->obtenerColumna($filaConsulta,'IdDistrito');
      $this->telefono = $this->obtenerColumna($filaConsulta,'Telefono');
    }

    public function bindValues($statement)
    {
      $statement->bindValue(1,$this->codigo,PDO::PARAM_INT);
      $statement->bindValue(2,$this->cod_libro,PDO::PARAM_INT);
      $statement->bindValue(3,$this->cod_foja,PDO::PARAM_INT);
      $statement->bindValue(4,$this->cod_numero,PDO::PARAM_INT);
      $statement->bindValue(5,$this->cod_parroquia,PDO::PARAM_STR);
      $statement->bindValue(6,$this->padre_nombre,PDO::PARAM_STR);
      $statement->bindValue(7,$this->padre_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(8,$this->padre_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(9,$this->nombre,PDO::PARAM_STR);
      $statement->bindValue(10,$this->madre_nombre,PDO::PARAM_STR);
      $statement->bindValue(11,$this->madre_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(12,$this->madre_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(13,$this->nacimiento,PDO::PARAM_STR);
      $statement->bindValue(14,$this->fnacimiento,PDO::PARAM_STR);
      $statement->bindValue(15,$this->fbautismo,PDO::PARAM_STR);
      $statement->bindValue(16,$this->madrina_nombre,PDO::PARAM_STR);
      $statement->bindValue(17,$this->madrina_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(18,$this->madrina_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(19,$this->padrino_nombre,PDO::PARAM_STR);
      $statement->bindValue(20,$this->padrino_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(21,$this->padrino_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(22,$this->anotaciones,PDO::PARAM_STR);
      $statement->bindValue(23,$this->sexo,PDO::PARAM_STR);
      $statement->bindValue(24,$this->naturalmadre,PDO::PARAM_STR);
      $statement->bindValue(25,$this->naturalpadre,PDO::PARAM_STR);
      $statement->bindValue(26,$this->cod_distrito,PDO::PARAM_INT);
      $statement->bindValue(27,$this->telefono,PDO::PARAM_STR);
      $statement->bindValue(28,$this->opcion,PDO::PARAM_INT);
		  $statement->bindValue(29,$this->pagina,PDO::PARAM_INT);
      return $statement;
    }

    public function set($metodo){
      $this->codigo = $metodo('IdBautismo');
      $this->cod_libro = $metodo('IdLibro');
      $this->cod_foja = $metodo('IdFoja');
      $this->cod_numero = $metodo('IdNumero');
      $this->cod_parroquia = $metodo('IdParroquia');
      $this->nombre = $metodo('Nombre');
      $this->padre_nombre = $metodo('PadreNombre');
      $this->padre_apPaterno = $metodo('PadreApellidoPaterno');
      $this->padre_apMaterno = $metodo('PadreApellidoMaterno');
      $this->madre_nombre = $metodo('MadreNombre');
      $this->madre_apPaterno = $metodo('MadreApellidoPaterno');
      $this->madre_apMaterno = $metodo('MadreApellidoMaterno');
      $this->nacimiento = $metodo('LugarNacimiento');
      $this->fnacimiento = $metodo('FechaNacimiento');
      $this->fbautismo = $metodo('FechaBautismo');
      $this->madrina_nombre = $metodo('MadrinaNombre');
      $this->madrina_apPaterno = $metodo('MadrinaApellidoPaterno');
      $this->madrina_apMaterno = $metodo('MadrinaApellidoMaterno');
      $this->padrino_nombre = $metodo('PadrinoNombre');
      $this->padrino_apPaterno = $metodo('PadrinoApellidoPaterno');
      $this->padrino_apMaterno = $metodo('PadrinoApellidoMaterno');
      $this->anotaciones = $metodo('Anotaciones');
      $this->sexo = $metodo('Sexo');
      $this->naturalmadre = $metodo('NaturalMadre');
      $this->naturalpadre = $metodo('NaturalPadre');
      $this->cod_distrito = $metodo('IdDistrito');
      $this->telefono = $metodo('Telefono');
    }
  }