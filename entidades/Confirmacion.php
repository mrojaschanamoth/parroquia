<?php

  namespace entidades;
  use PDO;

  class Confirmacion extends Entidad{
    public $codigo;
    public $cod_libro;
    public $cod_foja;
    public $cod_numero;
    public $cod_parroco;
    public $cod_padre;
    public $nombres;
    public $apellidos;
    public $padre_nombre;
    public $madre_nombre;
    public $fconfirmacion;
    public $padrino_nombre;
    public $confirmoen;
    public $edad;
    public $bautizoen;
    public $fbautizo;

    public function setConsulta($filaConsulta)
    {
      $this->codigo = $this->obtenerColumna($filaConsulta,'IdConfirmacion');
      $this->cod_libro = $this->obtenerColumna($filaConsulta,'Libro');
      $this->cod_foja = $this->obtenerColumna($filaConsulta,'Foja');
      $this->cod_numero = $this->obtenerColumna($filaConsulta,'Numero');
      $this->cod_parroco = $this->obtenerColumna($filaConsulta,'IdParroco');
      $this->cod_padre = $this->obtenerColumna($filaConsulta,'IdPadre');
      $this->nombres = $this->obtenerColumna($filaConsulta,'Nombre');
      $this->apellidos = $this->obtenerColumna($filaConsulta,'Apellidos');
      $this->padre_nombre = $this->obtenerColumna($filaConsulta,'PadreNombre');
      $this->madre_nombre = $this->obtenerColumna($filaConsulta,'MadreNombre');
      $this->confirmoen = $this->obtenerColumna($filaConsulta,'ConfirmoEn');
      $this->fconfirmacion = $this->obtenerColumna($filaConsulta,'FechaConfirmacion');
      $this->padrino_nombre = $this->obtenerColumna($filaConsulta,'PadrinoNombre');
      $this->edad = $this->obtenerColumna($filaConsulta,'Edad');
      $this->bautizoen = $this->obtenerColumna($filaConsulta,'BautizadoEn');
      $this->fbautizo = $this->obtenerColumna($filaConsulta,'FechaBautizo');
    }

    public function bindValues($statement)
    {
      $statement->bindValue(1,$this->codigo,PDO::PARAM_INT);
      $statement->bindValue(2,$this->cod_libro,PDO::PARAM_INT);
      $statement->bindValue(3,$this->cod_foja,PDO::PARAM_INT);
      $statement->bindValue(4,$this->cod_numero,PDO::PARAM_INT);
      $statement->bindValue(5,$this->cod_parroco,PDO::PARAM_INT);
      $statement->bindValue(6,$this->cod_padre,PDO::PARAM_STR);
      $statement->bindValue(7,$this->nombres,PDO::PARAM_STR);
      $statement->bindValue(8,$this->apellidos,PDO::PARAM_STR);
      $statement->bindValue(9,$this->padre_nombre,PDO::PARAM_STR);
      $statement->bindValue(10,$this->madre_nombre,PDO::PARAM_STR);
      $statement->bindValue(11,$this->fconfirmacion,PDO::PARAM_STR);
      $statement->bindValue(12,$this->padrino_nombre,PDO::PARAM_STR);
      $statement->bindValue(13,$this->confirmoen,PDO::PARAM_STR);
      $statement->bindValue(14,$this->edad,PDO::PARAM_INT);
      $statement->bindValue(15,$this->bautizoen,PDO::PARAM_STR);
      $statement->bindValue(16,$this->fbautizo,PDO::PARAM_STR);
      $statement->bindValue(17,$this->opcion,PDO::PARAM_INT);
      $statement->bindValue(18,$this->pagina,PDO::PARAM_INT);
      return $statement;
    }

    public function set($metodo){
      $this->codigo = $metodo('IdConfirmacion');
      $this->cod_libro = $metodo('Libro');
      $this->cod_foja = $metodo('Foja');
      $this->cod_numero = $metodo('Numero');
      $this->cod_parroco = $metodo('IdParroco');
      $this->cod_padre = $metodo('IdPadre');
      $this->nombres = $metodo('Nombre');
      $this->apellidos = $metodo('Apellidos');
      $this->padre_nombre = $metodo('PadreNombre');
      $this->madre_nombre = $metodo('MadreNombre');
      $this->fconfirmacion = $metodo('FechaConfirmacion');
      $this->padrino_nombre = $metodo('PadrinoNombre');
      $this->confirmoen = $metodo('ConfirmoEn');
      $this->edad = $metodo('Edad');
      $this->bautizoen = $metodo('BautizadoEn');
      $this->fbautizo = $metodo('FechaBautizo');
    }
  }