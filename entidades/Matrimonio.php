<?php

  namespace entidades;
  use PDO;

  class Matrimonio extends Entidad{

    public $codigo;
    public $cod_padre;
    public $cod_libro;
    public $cod_foja;
    public $cod_numero;
    public $cod_parroco;
    public $cod_parroquia;
    public $fmatrimonio; 
    public $novio_nombre;
    public $novio_apPaterno;
    public $novio_apMaterno;
    public $novio_ecivil;
    public $novio_nacimiento;
    public $novio_fnacimiento;
    public $novio_edad;
    public $novio_parroquiabautismo;
    public $novio_fbautismo;
    public $novio_madre_nombre;
    public $novio_madre_apPaterno;
    public $novio_madre_apMaterno;
    public $novio_padre_nombre;
    public $novio_padre_apPaterno;
    public $novio_padre_apMaterno; 
    public $novia_nombre;
    public $novia_apPaterno;
    public $novia_apMaterno;
    public $novia_ecivil;
    public $novia_nacimiento;
    public $novia_fnacimiento;
    public $novia_edad;
    public $novia_parroquiabautismo;
    public $novia_fbautismo;
    public $novia_madre_nombre;
    public $novia_madre_apPaterno;
    public $novia_madre_apMaterno;
    public $novia_padre_nombre;
    public $novia_padre_apPaterno;
    public $novia_padre_apMaterno;
    public $testigo1;
    public $testigo2;
    public $padrino;
    public $madrina;
    public $anotaciones;

    public function setConsulta($filaConsulta)
    {
      $this->codigo = $this->obtenerColumna($filaConsulta,'IdMatrimonio');
      $this->cod_padre = $this->obtenerColumna($filaConsulta,'IdPadre');
      $this->cod_libro = $this->obtenerColumna($filaConsulta,'IdLibro');
      $this->cod_foja = $this->obtenerColumna($filaConsulta,'IdFoja');
      $this->cod_numero = $this->obtenerColumna($filaConsulta,'IdNumero');
      $this->cod_parroco = $this->obtenerColumna($filaConsulta,'IdParroco');
      $this->cod_parroquia = $this->obtenerColumna($filaConsulta,'IdParroquia');
      $this->fmatrimonio = $this->obtenerColumna($filaConsulta,'FechaMatrimonio');
      $this->novio_nombre = $this->obtenerColumna($filaConsulta,'NombreNovio');
      $this->novio_apPaterno = $this->obtenerColumna($filaConsulta,'ApellidoPaternoNovio');
      $this->novio_apMaterno = $this->obtenerColumna($filaConsulta,'ApellidoMaternoNovio');
      $this->novio_ecivil = $this->obtenerColumna($filaConsulta,'EstadoCivilNovio');
      $this->novio_nacimiento = $this->obtenerColumna($filaConsulta,'LugarNacimientoNovio');
      $this->novio_fnacimiento = $this->obtenerColumna($filaConsulta,'FechaNacimientoNovio');
      $this->novio_edad = $this->obtenerColumna($filaConsulta,'EdadNovio');
      $this->novio_parroquiabautismo = $this->obtenerColumna($filaConsulta,'ParroquiaBautismoNovio');
      $this->novio_fbautismo = $this->obtenerColumna($filaConsulta,'FechaBautismoNovio');
      $this->novio_madre_nombre = $this->obtenerColumna($filaConsulta,'NombreMadreNovio');
      $this->novio_madre_apPaterno = $this->obtenerColumna($filaConsulta,'ApellidoPaternoMadreNovio');
      $this->novio_madre_apMaterno = $this->obtenerColumna($filaConsulta,'ApellidoMaternoMadreNovio');
      $this->novio_padre_nombre = $this->obtenerColumna($filaConsulta,'NombrePadreNovio');
      $this->novio_padre_apPaterno = $this->obtenerColumna($filaConsulta,'ApellidoPaternoNovio');
      $this->novio_padre_apMaterno = $this->obtenerColumna($filaConsulta,'ApellidoMaternoNovio');
      $this->novia_nombre = $this->obtenerColumna($filaConsulta,'NombreNovia');
      $this->novia_apPaterno = $this->obtenerColumna($filaConsulta,'ApellidoPaternoNovia');
      $this->novia_apMaterno = $this->obtenerColumna($filaConsulta,'ApellidoMaternoNovia');
      $this->novia_ecivil = $this->obtenerColumna($filaConsulta,'EstadoCivilNovia');
      $this->novia_nacimiento = $this->obtenerColumna($filaConsulta,'LugarNacimientoNovia');
      $this->novia_fnacimiento = $this->obtenerColumna($filaConsulta,'FechaNacimientoNovia');
      $this->novia_edad = $this->obtenerColumna($filaConsulta,'EdadNovia');
      $this->novia_parroquiabautismo = $this->obtenerColumna($filaConsulta,'ParroquiaBautismoNovia');
      $this->novia_fbautismo = $this->obtenerColumna($filaConsulta,'FechaBautismoNovia');
      $this->novia_madre_nombre = $this->obtenerColumna($filaConsulta,'NombreMadreNovia');
      $this->novia_madre_apPaterno = $this->obtenerColumna($filaConsulta,'ApellidoPaternoMadreNovia');
      $this->novia_madre_apMaterno = $this->obtenerColumna($filaConsulta,'ApellidoMaternoMadreNovia');
      $this->novia_padre_nombre = $this->obtenerColumna($filaConsulta,'NombrePadreNovia');
      $this->novia_padre_apPaterno = $this->obtenerColumna($filaConsulta,'ApellidoPaternoPadreNovia');
      $this->novia_padre_apMaterno = $this->obtenerColumna($filaConsulta,'ApellidoMaternoMadreNovia');
      $this->testigo1 = $this->obtenerColumna($filaConsulta,'Testigo1');
      $this->testigo2 = $this->obtenerColumna($filaConsulta,'Testigo2');
      $this->padrino = $this->obtenerColumna($filaConsulta,'Padrino');
      $this->madrina = $this->obtenerColumna($filaConsulta,'Madrina');
      $this->anotaciones = $this->obtenerColumna($filaConsulta,'Anotaciones');

    }

    public function bindValues($statement)
    {
      $statement->bindValue(1,$this->codigo,PDO::PARAM_INT);
      $statement->bindValue(2,$this->cod_padre,PDO::PARAM_INT);
      $statement->bindValue(3,$this->cod_libro,PDO::PARAM_INT);
      $statement->bindValue(4,$this->cod_foja,PDO::PARAM_INT);
      $statement->bindValue(5,$this->cod_numero,PDO::PARAM_INT);
      $statement->bindValue(6,$this->cod_parroco,PDO::PARAM_INT);
      $statement->bindValue(7,$this->cod_parroquia,PDO::PARAM_INT);
      $statement->bindValue(8,$this->fmatrimonio,PDO::PARAM_STR);
      $statement->bindValue(9,$this->novio_nombre,PDO::PARAM_STR);
      $statement->bindValue(10,$this->novio_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(11,$this->novio_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(12,$this->novio_ecivil,PDO::PARAM_STR);
      $statement->bindValue(13,$this->novio_nacimiento,PDO::PARAM_STR);
      $statement->bindValue(14,$this->novio_fnacimiento,PDO::PARAM_STR);
      $statement->bindValue(15,$this->novio_edad,PDO::PARAM_INT);
      $statement->bindValue(16,$this->novio_parroquiabautismo,PDO::PARAM_STR);
      $statement->bindValue(17,$this->novio_fbautismo,PDO::PARAM_STR);
      $statement->bindValue(18,$this->novio_madre_nombre,PDO::PARAM_STR);
      $statement->bindValue(19,$this->novio_madre_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(20,$this->novio_madre_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(21,$this->novio_padre_nombre,PDO::PARAM_STR);
      $statement->bindValue(22,$this->novio_padre_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(23,$this->novio_padre_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(24,$this->novia_nombre,PDO::PARAM_STR);
      $statement->bindValue(25,$this->novia_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(26,$this->novia_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(27,$this->novia_ecivil,PDO::PARAM_STR);
      $statement->bindValue(28,$this->novia_nacimiento,PDO::PARAM_STR);
      $statement->bindValue(29,$this->novia_fnacimiento,PDO::PARAM_STR);
      $statement->bindValue(30,$this->novia_edad,PDO::PARAM_INT);
      $statement->bindValue(31,$this->novia_parroquiabautismo,PDO::PARAM_STR);
      $statement->bindValue(32,$this->novia_fbautismo,PDO::PARAM_STR);
      $statement->bindValue(33,$this->novia_madre_nombre,PDO::PARAM_STR);
      $statement->bindValue(34,$this->novia_madre_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(35,$this->novia_madre_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(36,$this->novia_padre_nombre,PDO::PARAM_STR);
      $statement->bindValue(37,$this->novia_padre_apPaterno,PDO::PARAM_STR);
      $statement->bindValue(38,$this->novia_padre_apMaterno,PDO::PARAM_STR);
      $statement->bindValue(39,$this->testigo1,PDO::PARAM_STR);
      $statement->bindValue(40,$this->testigo2,PDO::PARAM_STR);
      $statement->bindValue(41,$this->padrino,PDO::PARAM_STR);
      $statement->bindValue(42,$this->madrina,PDO::PARAM_STR);
      $statement->bindValue(43,$this->anotaciones ,PDO::PARAM_STR);
      $statement->bindValue(44,$this->opcion,PDO::PARAM_INT);
      $statement->bindValue(45,$this->pagina,PDO::PARAM_INT);

      return $statement;
    }

    public function set($metodo){
      $this->cod_libro = $metodo('IdLibro');
      $this->cod_foja = $metodo('IdFoja');
      $this->cod_numero = $metodo('IdNumero');
      $this->cod_parroco = $metodo('IdParroco');
      $this->cod_parroquia = $metodo('IdParroquia');
      $this->fmatrimonio = $metodo('FechaMatrimonio');
      $this->novio_nombre = $metodo('NombreNovio');
      $this->novio_apPaterno = $metodo('ApellidoPaternoNovio');
      $this->novio_apMaterno = $metodo('ApellidoMaternoNovio');
      $this->novio_ecivil = $metodo('EstadoCivilNovio');
      $this->novio_nacimiento = $metodo('LugarNacimientoNovio');
      $this->novio_fnacimiento = $metodo('FechaNacimientoNovio');
      $this->novio_edad = $metodo('EdadNovio');
      $this->novio_parroquiabautismo = $metodo('ParroquiaBautismoNovio');
      $this->novio_fbautismo = $metodo('FechaBautismoNovio');
      $this->novio_madre_nombre = $metodo('NombreMadreNovio');
      $this->novio_madre_apPaterno = $metodo('ApellidoPaternoMadreNovio');
      $this->novio_madre_apMaterno = $metodo('ApellidoMaternoMadreNovio');
      $this->novio_padre_nombre = $metodo('NombrePadreNovio');
      $this->novio_padre_apPaterno = $metodo('ApellidoPaternoNovio');
      $this->novio_padre_apMaterno = $metodo('ApellidoMaternoNovio');
      $this->novia_nombre = $metodo('NombreNovia');
      $this->novia_apPaterno = $metodo('ApellidoPaternoNovia');
      $this->novia_apMaterno = $metodo('ApellidoMaternoNovia');
      $this->novia_ecivil = $metodo('EstadoCivilNovia');
      $this->novia_nacimiento = $metodo('LugarNacimientoNovia');
      $this->novia_fnacimiento = $metodo('FechaNacimientoNovia');
      $this->novia_edad = $metodo('EdadNovia');
      $this->novia_parroquiabautismo = $metodo('ParroquiaBautismoNovia');
      $this->novia_fbautismo = $metodo('FechaBautismoNovia');
      $this->novia_madre_nombre = $metodo('NombreMadreNovia');
      $this->novia_madre_apPaterno = $metodo('ApellidoPaternoMadreNovia');
      $this->novia_madre_apMaterno = $metodo('ApellidoMaternoMadreNovia');
      $this->novia_padre_nombre = $metodo('NombrePadreNovia');
      $this->novia_padre_apPaterno = $metodo('ApellidoPaternoPadreNovia');
      $this->novia_padre_apMaterno= $metodo('ApellidoMaternoMadreNovia');
      $this->testigo1 = $metodo('Testigo1');
      $this->testigo2 = $metodo('Testigo2');
      $this->padrino = $metodo('Padrino');
      $this->madrina = $metodo('Madrina');
      $this->anotaciones = $metodo('Anotaciones');
      $this->codigo = $metodo('IdMatrimonio');
      $this->cod_padre = $metodo('IdPadre');
    }
  }
  