<?php

  namespace entidades;
  use PDO;

  class Parroco extends Entidad{
    public $codigo;
    public $nombre;
    public $carnet;

    public function setConsulta($filaConsulta)
    {
      $this->codigo = $this->obtenerColumna($filaConsulta, 'IdParroco');
      $this->nombre = $this->obtenerColumna($filaConsulta,'Nombre');
      $this->carnet = $this->obtenerColumna($filaConsulta, 'Carnet');
    }

    public function bindValues($statement)
    {
      $statement->bindValue(1,$this->codigo,PDO::PARAM_INT);
      $statement->bindValue(2,$this->nombre,PDO::PARAM_STR);
      $statement->bindValue(3,$this->carnet,PDO::PARAM_STR);
      $statement->bindValue(4,$this->opcion,PDO::PARAM_INT);
		  $statement->bindValue(5,$this->pagina,PDO::PARAM_INT);
      return $statement;
    }

    public function set($metodo){
      $this->codigo = $metodo('IdParroco');
      $this->nombre = $metodo('Nombre');
      $this->carnet = $metodo('Carnet');
    }
  }