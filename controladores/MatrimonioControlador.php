<?php

  namespace controladores;
  use \Mpdf\Mpdf;

  class MatrimonioControlador extends Controlador{
    private $mpdf;

    public function __construct()
    {
      $this->mpdf = new Mpdf();
      parent::__construct('MatrimonioModelo','Matrimonio','vistas/matrimonios/index.php');
    }

    public function crear(){
      $this->entidad->setMetodoPost();
      var_dump($this->entidad);exit();
      $respuesta=$this->modelo->crear($this->entidad);
      $this->index();
    }

    public function created(){
      $vista = 'vistas/matrimonios/crear.php';
      require_once 'vistas/plantilla/index.php';
    }

    public function get(){
      $this->entidad->setMetodoGet();
      $respuesta=$this->modelo->get($this->entidad);
      $contenido = $respuesta->resultado;
      $vista = "vistas/matrimonios/editar.php";
     require_once 'vistas/plantilla/index.php';
    }

    public function editar(){
      $this->entidad->setMetodoPost();
      $respuesta=$this->modelo->editar($this->entidad);
      $this->index();  
    }

    public function generar_constancia(){
      $this->entidad->setMetodoGet();
      $contenido = ($this->modelo->get($this->entidad))->resultado;
      require_once "vistas/matrimonios/formato.php";
      $this->mpdf->WriteHTML($constancia);
      $this->mpdf->Output();
    }
  }