<?php

  namespace controladores;

  class UsuarioControlador extends Controlador{

    public function __construct()
    {
      parent::__construct('UsuarioModelo','Usuario','vistas/usuarios/index.php');
    }

    public function login(){
      if(isset($_POST['Usuario'],$_POST['Clave'])){
        $this->entidad->setMetodoPost();

        $respuesta = $this->modelo->login($this->entidad);

        if($respuesta->respuesta){
          $respuesta->resultado->iniciarSesion();
          $this->respuesta($respuesta);
        }else{
          $respuesta->mensaje = "El usuario no existe";
          $this->respuesta($respuesta);
        }
      }else{
        require_once 'vistas/auth/login.php';
      }
    }

    public function logout(){
      unset($_SESSION['codigo']);
      unset($_SESSION['usuario']);
      unset($_SESSION['nombre']);

      session_destroy();
      header('Location:index.php');
      exit();
    }

  }