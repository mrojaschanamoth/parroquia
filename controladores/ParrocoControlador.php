<?php

  namespace controladores;

  class ParrocoControlador extends Controlador{

    public function __construct()
    {
      parent::__construct('ParrocoModelo','Parroco','vistas/parrocos/index.php');
    }

    public function index(){
      $respuesta=$this->modelo->listarAll();
      $vista=$this->vista;
      require_once 'vistas/plantilla/index.php';
    }

  }