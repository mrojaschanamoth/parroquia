<?php

  namespace controladores;
  use \Mpdf\Mpdf;

  class ConfirmacionControlador extends Controlador{
    private $mpdf;
    public function __construct()
    {
      $this->mpdf = new Mpdf();
      parent::__construct('ConfirmacionModelo','Confirmacion','vistas/confirmaciones/index.php');
    }
    public function index(){
      $respuesta=$this->modelo->listarAll();
      $vista=$this->vista;
      require_once 'vistas/plantilla/index.php';
    }
    public function get_editar(){
      $this->entidad->setMetodoGet();
      $respuesta=$this->modelo->get($this->entidad);
      $contenido = $respuesta->resultado;
      $vista = "vistas/confirmaciones/editar.php";
      require_once 'vistas/plantilla/index.php';
    }
    public function editar(){
      $this->entidad->setMetodoPost();
      $respuesta=$this->modelo->editar($this->entidad);
      $this->index();
    }
    public function generar_constancia(){
      $this->entidad->setMetodoGet();
      $contenido = ($this->modelo->get($this->entidad))->resultado;
      require_once "vistas/confirmaciones/formato.php";
      $this->mpdf->WriteHTML($constancia);
      $this->mpdf->Output();
    }
  }