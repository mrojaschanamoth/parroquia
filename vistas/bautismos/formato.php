<?php

$constancia = <<<PDF
  
  <body>
    
    <div>
      <div><p style="opacity: 0.7;">&nbsp;&nbsp;&nbsp;</p></div>
      <div style="margin-left: 6cm; margin-top: 1.5cm;">$contenido->cod_parroquia</div>
      <div style="margin-left: 10.5cm; margin-top: 0.4cm;">$contenido->cod_libro
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      $contenido->cod_foja
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      $contenido->cod_numero</div>
      <div style="margin-left: 4cm; margin-top: 0.4cm;">$contenido->padre_apPaterno $contenido->madre_apPaterno</div>
      <div style="margin-left: 4cm; margin-top: 0.4cm;">$contenido->nombre</div>
      <div style="margin-left: 3.5cm; margin-top: 0.4cm;">$contenido->padre_apPaterno $contenido->padre_apMaterno $contenido->padre_nombre</div>
      <div style="margin-left: 3.5cm; margin-top: 0.4cm;">$contenido->madre_apPaterno $contenido->madre_apMaterno $contenido->madre_nombre</div>
      <div style="margin-left: 7cm; margin-top: 0.4cm;">$contenido->nacimiento | $contenido->fnacimiento</div>
      <div style="margin-left: 5.5cm; margin-top: 0.4cm;">$contenido->fbautismo</div>
      <div style="margin-left: 4cm; margin-top: 0.4cm;">$contenido->padrino_apPaterno $contenido->padrino_apMaterno $contenido->padrino_nombre | 
      $contenido->madrina_apPaterno $contenido->madrina_apMaterno $contenido->madrina_nombre</div>
      <div style="margin-left: 6.5cm; margin-top: 0.4cm;">$contenido->anotaciones</div>
      
    </div>    
  </body>
';
PDF;
