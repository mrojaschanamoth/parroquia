<form method="post" action="?c=bautismo&a=crear" id="formCrear" autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title">Registro de Bautismo Nuevo</h5>
  </div>

  <div class="modal-body" style="height:450px;overflow-y: scroll;">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="IdLibro">Codigo de Libro:</label>
          <input type="text" name="IdLibro" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="IdFoja">C贸digo de Foja:</label>
          <input type="text" name="IdFoja" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="IdNumero">C贸digo de Numero:</label>
          <input type="text" name="IdNumero" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="IdParroquia">C贸digo de Parroquia:</label>
          <input type="text" name="IdParroquia" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="Nombre">Nombre del Bautismo:</label>
          <input type="text" name="Nombre" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="LugarNacimiento">Lugar de Nacimiento:</label>
          <input type="text" name="LugarNacimiento" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="FechaNacimiento">Fecha de Nacimiento:</label>
          <input type="date" name="FechaNacimiento" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="FechaBautismo">Fecha de Bautismo:</label>
          <input type="date" name="FechaBautismo" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>
    <hr>
    <h5>Informacion de los padres</h5>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadreNombre">Nombre del padre:</label>
          <input type="text" name="PadreNombre " class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadreApellidoPaterno">Apellido Paterno del padre:</label>
          <input type="text" name="PadreApellidoPaterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadreApellidoMaterno ">Apellido materno del padre:</label>
          <input type="text" name="PadreApellidoMaterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadreNombre">Nombre de la madre:</label>
          <input type="text" name="MadreNombre " class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="MadreApellidoPaterno">Apellido Paterno de la madre:</label>
          <input type="text" name="MadreApellidoPaterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="MadreApellidoMaterno ">Apellido materno de la madre:</label>
          <input type="text" name="MadreApellidoMaterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <hr>
      <h5>Informacion de los Padrinos</h5>
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadrinoNombre">Nombre del padrino:</label>
          <input type="text" name="PadrinoNombre " class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadrinoApellidoPaterno">Apellido Paterno del padrino:</label>
          <input type="text" name="PadrinoApellidoPaterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadrinoApellidoMaterno ">Apellido materno del padrino:</label>
          <input type="text" name="PadrinoApellidoMaterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="MadrinaNombre">Nombre de la madrina:</label>
          <input type="text" name="MadrinaNombre " class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="MadrinaApellidoPaterno">Apellido Paterno de la madrina:</label>
          <input type="text" name="MadrinaApellidoPaterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="MadrinaApellidoMaterno ">Sexo:</label>
          <input type="text" name="MadrinaApellidoMaterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="NaturalMadre ">Madre natural de:</label>
          <input type="text" name="NaturalMadre" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="NaturalPadre ">Padre natural de:</label>
          <input type="text" name="NaturalPadre" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
  </div>

  <div class="modal-footer">
    <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar</button>
  </div>
</form>