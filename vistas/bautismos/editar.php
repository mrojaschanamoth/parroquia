<section class="wrapper site-min-height">
  <h3><i class="fa fa-angle-right"></i> Editar Informaci贸n de Bautismo de : <strong><?php echo $contenido->nombre;?></strong></h3>
  <div class="content-panel">
    <form method="post" action="?c=bautismo&a=editar" autocomplete="off">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdLibro">Codigo de Libro:</label>
              <input type="text" name="IdLibro" class="form-control" value="<?php echo $contenido->cod_libro?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdFoja">C贸digo de Foja:</label>
              <input type="text" name="IdFoja" class="form-control" value="<?php echo $contenido->cod_foja?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdNumero">C贸digo de Numero:</label>
              <input type="text" name="IdNumero" class="form-control" value="<?php echo $contenido->cod_numero?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdParroquia">C贸digo de Parroquia:</label>
              <input type="text" name="IdParroquia" class="form-control" value="<?php echo $contenido->cod_parroquia?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label for="Nombre">Nombre del Bautismo:</label>
              <input type="text" name="Nombre" class="form-control" value="<?php echo $contenido->nombre?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="LugarNacimiento">Lugar de Nacimiento:</label>
              <input type="text" name="LugarNacimiento" class="form-control" value="<?php echo $contenido->nacimiento?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="FechaNacimiento">Fecha de Nacimiento:</label>
              <input type="date" name="FechaNacimiento" class="form-control" value="<?php echo $contenido->fnacimiento?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="FechaBautismo">Fecha de Bautismo:</label>
              <input type="date" name="FechaBautismo" class="form-control" value="<?php echo $contenido->fbautismo?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
        </div>
        <hr>
        <h5>Informacion de los padres</h5>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreNombre">Nombre del padre:</label>
              <input type="text" name="PadreNombre" class="form-control" value="<?php echo $contenido->padre_nombre?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreApellidoPaterno">Apellido Paterno del padre:</label>
              <input type="text" name="PadreApellidoPaterno" class="form-control" value="<?php echo $contenido->padre_apPaterno?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreApellidoMaterno ">Apellido materno del padre:</label>
              <input type="text" name="PadreApellidoMaterno" class="form-control" value="<?php echo $contenido->padre_apMaterno?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreNombre">Nombre de la madre:</label>
              <input type="text" name="MadreNombre" class="form-control" value="<?php echo $contenido->madre_nombre?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadreApellidoPaterno">Apellido Paterno de la madre:</label>
              <input type="text" name="MadreApellidoPaterno" class="form-control" value="<?php echo $contenido->madre_apPaterno?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadreApellidoMaterno ">Apellido materno de la madre:</label>
              <input type="text" name="MadreApellidoMaterno" class="form-control" value="<?php echo $contenido->madre_apMaterno?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
        </div>
          <hr>
          <h5>Informacion de los Padrinos</h5>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadrinoNombre">Nombre del padrino:</label>
              <input type="text" name="PadrinoNombre" class="form-control" value="<?php echo $contenido->padrino_nombre?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadrinoApellidoPaterno">Apellido Paterno del padrino:</label>
              <input type="text" name="PadrinoApellidoPaterno" class="form-control" value="<?php echo $contenido->padrino_apPaterno?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadrinoApellidoMaterno ">Apellido materno del padrino:</label>
              <input type="text" name="PadrinoApellidoMaterno" class="form-control" value="<?php echo $contenido->padrino_apMaterno?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadrinaNombre">Nombre de la madrina:</label>
              <input type="text" name="MadrinaNombre" class="form-control" value="<?php echo $contenido->madrina_nombre?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadrinaApellidoPaterno">Apellido Paterno de la madrina:</label>
              <input type="text" name="MadrinaApellidoPaterno" class="form-control" value="<?php echo $contenido->madrina_apPaterno?>" onKeyUp="this.value=this.value.toUpperCase();"> 
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadrinaApellidoMaterno ">Apellido Materno de la madrina:</label>
              <input type="text" name="MadrinaApellidoMaterno" class="form-control" value="<?php echo $contenido->madrina_apMaterno?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="NaturalMadre ">Madre natural de:</label>
              <input type="text" name="NaturalMadre" class="form-control" value="<?php echo $contenido->naturalmadre?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="NaturalPadre ">Padre natural de:</label>
              <input type="text" name="NaturalPadre" class="form-control" value="<?php echo $contenido->naturalpadre?>" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
        </div>

    <input type="hidden" name="Sexo" value="<?php echo $contenido->sexo?>" onKeyUp="this.value=this.value.toUpperCase();">
        <input type="hidden" name="IdBautismo" value="<?php echo $contenido->codigo?>">
        <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar cambios</button>
    </form>
  </div>
</section>
