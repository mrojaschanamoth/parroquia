<section class="wrapper">
  <h3><i class="fa fa-tasks"></i> Usuarios</h3><br>

  <!--<div class="showback showback-theme">
     <div class="col-md-offset-11">
     <button type="button" class="btn btn-theme" data-toggle="modal" data-target="#crearModal"><i class="fa fa-cog"></i> Nuevo</button>
     </div>
  </div>-->


  <div class="row">
  <div class="col-lg-12">
  <div class="panel panel-border panel-warning widget-s-1">
  <div class="panel-heading">
      <h4><i class="fa fa-search"></i> Listado<span class="pull-right">

<div class="btn-group dropdown">
<button type="button" class="btn btn-primary waves-effect waves-light"><span class="fa fa-cog"></span> Procesos</button>
<button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><i class="caret"></i></button>
<ul class="dropdown-menu" role="menu">
<li><a href="#" data-toggle="modal" data-target="#crearModal"><i class="fa fa-plus"></i> Nuevo Registro</a></li>
</ul>
</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                 
</span>

</h4> </div>
      <div class="panel-body">
        <?php require 'tabla.php' ?>
      </div>
  </div>
  </div>
  </div>
  </div>
</section>

<div class="modal fade" id="crearModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php require 'crear.php';   ?>
    </div>
  </div>
</div>

<div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <?php require 'editar.php';   ?>
    </div>
  </div>
</div>

<script type="text/javascript">
  function funcionOpcionEditar(usuario){
    $('#formEditar input[name="Usuario"]').val(usuario.user);
    $('#formEditar input[name="IdUsuario"]').val(usuario.codigo);
    $('#formEditar input[name="Nombre"]').val(usuario.nombre);
    $('#formEditar input[name="apPaterno"]').val(usuario.apPaterno);
    $('#formEditar input[name="apMaterno"]').val(usuario.apMaterno);
    $('#formEditar input[name="Clave"]').val(usuario.Clave);
    $('#formEditar input[name="direccion"]').val(usuario.direccion);
  }
</script>



