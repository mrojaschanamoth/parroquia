<form method="post" action="?c=usuario&a=editar" id="formEditar" autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title">Editar Registro</h5>
  </div>

  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="Nombre">Nombre completo:</label>
          <input type="text" name="Nombre" placeholder="Ingrese los nombres" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group ">
          <label for="apPaterno">Apellido Paterno</label>
          <input type="text" name="apPaterno" placeholder="Ingrese apellido paterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group ">
          <label for="apMaterno">Apellido Materno</label>
          <input type="text" name="apMaterno" placeholder="Ingrese apellido materno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="direccion">Direcci贸n:</label>
          <input type="text" name="direccion" placeholder="Ingrese direccion" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group ">
          <label for="IdUsuario">Usuario:</label>
          <input type="text" name="Usuario" class="form-control" placeholder="Ingrese usuario" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group ">
          <label for="Clave">Contrase帽a</label>
          <input type="password" name="Clave" class="form-control" placeholder="Ingrese clave" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>
  </div>

  <div class="modal-footer">
  <input type="hidden" name="IdUsuario">
    <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar Cambios</button>
  </div>
</form>