<form method="post" action="?c=usuario&a=crear" id="formCrear" autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Registro de Usuario Nuevo</h5>
  </div>

  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="Nombre">Nombre completo:</label>
          <input type="text" name="Nombre" placeholder="Ingrese nombres" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group ">
          <label for="apPaterno">Apellido Paterno</label>
          <input type="text" name="apPaterno" placeholder="Ingrese apellido paterno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group ">
          <label for="apMaterno">Apellido Materno</label>
          <input type="text" name="apMaterno" placeholder="Ingrese apellido materno" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="direccion">Direcci贸n:</label>
          <input type="text" name="direccion" placeholder="Ingrese direccion" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group ">
          <label for="IdUsuario">Usuario:</label>
          <input type="text" name="Usuario" placeholder="Ingrese usuario" required class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group ">
          <label for="Clave">Contrase帽a</label>
          <input type="password" name="Clave" placeholder="Ingrese clave" required class="form-control" placeholder="*******" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>
  </div>

  <div class="modal-footer">
    <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar</button>
  </div>
</form>