<div class="table-responsive">
<table class="table table-hover table-responsive" id="tusuarios">           
  <thead>
    <tr>
      <th>#</th>
      <!--<th>Nombres</th>-->
      <th>Apellidos y Nombres</th>
      <th>Usuario</th>
      <th width="10%">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
      if ($respuesta->respuesta){
        $filas = $respuesta->resultado->objetos;
        $cont = 1;
        foreach($filas as $fila): ?>
           <tr>
             <td><b><?php echo $cont; $cont++; ?></b></td>
             <!--<td><?php echo $fila->nombre; ?></td>-->
             <td><?php echo $fila->apPaterno.' '.$fila->apMaterno.", ".$fila->nombre; ?></td>
             <td><?php echo $fila->user; ?></td>
             <td>
                <a  <?php echo "href='?c=usuario&a=get&IdUsuario=$fila->codigo'"; ?> class="btn  btn-sm  btn-warning editar"><i class="fa fa-edit"></i> </a>
                <a <?php echo "href='?c=usuario&a=eliminar&IdUsuario=$fila->codigo'"; ?>  class="btn  btn-sm  btn-danger  eliminar" ><i class="fa fa-trash"></i> </a>
             </td>
           </tr>          
        <?php  endforeach;
      }else{ ?>
          
          <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error!</strong><br>
            <span>Ocurri贸 un problema al cargar la tabla</span>
           
          </div>
          
     <?php }
    ?>
  </tbody>
</table>
</div>