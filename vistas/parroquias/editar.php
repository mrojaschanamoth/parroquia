<form method="post" action="?c=parroquia&a=editar" id="formEditar" autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Editar Registro</h5>
  </div>

  <div class="modal-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="IdParroquia">C贸digo de la Parroqu铆a:</label>
          <input type="text" name="IdParroquia" class="form-control" readonly>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="IdDistrito">C贸digo del Distrito:</label>
          <input type="text" name="IdDistrito" placeholder="Ingrese codigo (maximo 2 digitos)" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="Nombre">Nombre completo:</label>
          <input type="text" name="Nombre" placeholder="Ingrese nombre" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="Direccion">Direccion:</label>
          <input type="text" name="Direccion" placeholder="Ingrese direccion" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="Telefono">Telefono:</label>
          <input type="text" name="Telefono" placeholder="Ingrese telefono" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>
  </div>

  <div class="modal-footer">
  <input type="hidden" name="IdParroquia">
    <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar</button>
  </div>
</form>