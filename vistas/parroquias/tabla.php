<div class="table-responsive">
<table class="table table-hover table-responsive" id="tparroquia">        
  <thead>
    <tr>
      <th>#</th>
      <th>Codigo</th>
      <th>Nombre</th>
      <th>Direccion</th>
      <th>Telefono</th>
      <th width="10%">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
      if ($respuesta->respuesta){
        $i = 1;
        $filas = $respuesta->resultado->objetos;
        foreach($filas as $fila): ?>
           <tr>
             <td><b><?php echo $i; $i++;?></b></td>
             <td><?php echo $fila->codigo; ?></td>
             <td><?php echo $fila->nombre; ?></td>
             <td><?php echo $fila->direccion; ?></td>
             <td><?php echo $fila->telefono; ?></td>
             <td>
                <a  <?php echo "href='?c=parroquia&a=get&IdParroquia=$fila->codigo'"; ?> class="btn  btn-sm  btn-warning editar"><i class="fa fa-edit"></i> </a>
                <a <?php echo "href='?c=parroquia&a=eliminar&IdParroquia=$fila->codigo'"; ?>  class="btn  btn-sm  btn-danger  eliminar" ><i class="fa fa-trash"></i> </a>
             </td>
           </tr>          
        <?php  endforeach;
      }else{ ?>
          
          <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error!</strong><br>
            <span>Ocurrió un problema al cargar la tabla</span>
           
          </div>
          
     <?php }
    ?>
  </tbody>
</table>
</div>