<section class="wrapper site-min-height">
  <h3><i class="fa fa-angle-right"></i> Editar Información de Confirmación de : <strong><?php echo $contenido->nombres;?></strong></h3>
  <div class="content-panel">
    
<form method="post" action="?c=confirmacion&a=editar"  autocomplete="off">
  <div class="modal-body">
    <div class="row">
      <div class="col-md-2">
        <div class="form-group">
          <label for="Libro">Código de Libro:</label>
          <input type="text" name="Libro" class="form-control" value="<?php echo $contenido->cod_libro?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label for="Foja">Código de Foja:</label>
          <input type="text" name="Foja" class="form-control" value="<?php echo $contenido->cod_foja?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label for="Numero">Código de Numero:</label>
          <input type="text" name="Numero" class="form-control" value="<?php echo $contenido->cod_numero?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label for="IdParroco">Código de Parroco:</label>
          <input type="text" name="IdParroco" class="form-control" value="<?php echo $contenido->cod_parroco?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label for="IdPadre">Código de Padre:</label>
          <input type="text" name="IdPadre" class="form-control" value="<?php echo $contenido->cod_padre?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="Nombre">Nombres:</label>
          <input type="text" name="Nombre" class="form-control" value="<?php echo $contenido->nombres?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="Nombre">Apellidos:</label>
          <input type="text" name="Apellidos" class="form-control" value="<?php echo $contenido->apellidos?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="Edad">Edad:</label>
          <input type="text" name="Edad" class="form-control" value="<?php echo $contenido->edad?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="FechaBautismo">Fecha de Confirmacion:</label>
          <input type="date" name="FechaConfirmacion" class="form-control" value="<?php echo $contenido->fconfirmacion?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>
    <hr>
    <h5>Informacion de los padres</h5>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadreNombre">Nombre completo del padre:</label>
          <input type="text" name="PadreNombre" class="form-control" value="<?php echo $contenido->padre_nombre?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadreNombre">Nombre completo de la madre:</label>
          <input type="text" name="MadreNombre" class="form-control" value="<?php echo $contenido->madre_nombre?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>
      <hr>
      <h5>Informacion de los Padrinos</h5>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadrinoNombre">Nombre del padrino:</label>
          <input type="text" name="PadrinoNombre" class="form-control" value="<?php echo $contenido->padrino_nombre?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="ConfirmoEn">Confirmo en::</label>
          <input type="date" name="ConfirmoEn" class="form-control" value="<?php echo $contenido->confirmoen?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="BautizadoEn">Bautizo en::</label>
          <input type="date" name="BautizadoEn" class="form-control" value="<?php echo $contenido->bautizoen?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="FechaBautizo">Fecha de Bautizo:</label>
          <input type="date" name="FechaBautizo" class="form-control" value="<?php echo $contenido->fbautizo?>" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>

<input type="hidden" name="Sexo" value="<?php echo $contenido->sexo?>">
    <input type="hidden" name="IdConfirmacion" value="<?php echo $contenido->codigo?>">
    <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar cambios</button>
</form>
  </div>
</section>
