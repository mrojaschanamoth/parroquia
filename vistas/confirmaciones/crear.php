<form method="post" action="?c=confirmacion&a=crear" id="formCrear" autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title">Registro de Nueva Confirmacion</h5>
  </div>

  <div class="modal-body">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="Libro">Codigo de Libro:</label>
          <input type="text" name="Libro" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Foja">C贸digo de Foja:</label>
          <input type="text" name="Foja" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Numero">C贸digo de Numero:</label>
          <input type="text" name="Numero" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="IdPadre">Parroco:</label>
          <input type="text" name="IdPadre" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="Nombre">Nombres:</label>
          <input type="text" name="Nombre" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="Apellidos">Apellidos:</label>
          <input type="text" name="Apellidos" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadreNombre">Nombre Completo del padre:</label>
          <input type="text" name="PadreNombre" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="MadreNombre">Nombre Completo de la madre:</label>
          <input type="text" name="MadreNombre" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="FechaConfirmacion">Fecha de Confirmaci贸n:</label>
          <input type="date" name="FechaConfirmacion" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="PadrinoNombre">Nombre completo del padrino:</label>
          <input type="text" name="PadrinoNombre" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="ConfirmoEn">Confirmo En:</label>
          <input type="date" name="ConfirmoEn" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="FechaBautizo">Fecha de Bautismo:</label>
          <input type="date" name="FechaBautizo" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="BautizadoEn">Bautizo En:</label>
          <input type="date" name="BautizadoEn" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="Edad">Edad:</label>
          <input type="text" name="Edad" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>

  <div class="modal-footer">
    <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar</button>
  </div>
</form>