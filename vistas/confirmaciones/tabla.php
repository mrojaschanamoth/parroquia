<div class="table-responsive">
<table class="table table-hover table-responsive" id="tconfirmacion">
  <thead>
    <tr>
      <th>#</th>
      <th>Libro</th>
      <th>Foja</th>
      <th>Numero</th>
      <th>Apellidos y Nombres</th>
      <!--<th>Apellidos</th>-->
      <th width="15%">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
      if ($respuesta->respuesta){
        $i = 1;
        $filas = $respuesta->resultado;
        foreach($filas as $fila): ?>
           <tr>
             <td><b><?php echo $i; $i++;?></b></td>
             <td><?php echo $fila->cod_libro; ?></td>
             <td><?php echo $fila->cod_foja; ?></td>
             <td><?php echo $fila->cod_numero; ?></td>
             <td><?php echo $fila->apellidos.", ".$fila->nombres ; ?></td>
             <!--<td><?php echo $fila->apellidos; ?></td>-->             
             <td>
                <a href="?c=confirmacion&a=generar_constancia&IdConfirmacion=<?php echo $fila->codigo ?>" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-file-text"></i></a>

                <a href="?c=confirmacion&a=get_editar&IdConfirmacion=<?php echo $fila->codigo ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                <a href="?c=confirmacion&a=eliminar&IdConfirmacion=<?php echo $fila->codigo ?>" class="btn btn-sm btn-danger eliminar"><i class="fa fa-trash"></i> </a>
             </td>
           </tr>          
        <?php  endforeach;
      }else{ ?>
          
          <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error!</strong><br>
            <span>Ocurrió un problema al cargar la tabla</span>
           
          </div>
          
     <?php }
    ?>
  </tbody>
</table>
</div>