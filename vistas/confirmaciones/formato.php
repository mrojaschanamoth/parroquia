<?php

$constancia = <<<PDF
  
  <body>
    
    <div>
      <div><p style="opacity: 0.7;">&nbsp;&nbsp;&nbsp;</p></div>
      <div style="margin-left: 4cm; margin-top: 1.5cm;">$contenido->apellidos</div>
      <div style="margin-left: 4cm; margin-top: 0.4cm;">$contenido->nombres</div>
      <div style="margin-left: 3.5cm; margin-top: 0.4cm;">$contenido->padre_nombre</div>
      <div style="margin-left: 3.5cm; margin-top: 0.4cm;">$contenido->madre_nombre</div>
      <div style="margin-left: 5cm; margin-top: 0.4cm;">$contenido->confirmoen</div>
      <div style="margin-left: 4.5cm; margin-top: 0.4cm;">$contenido->edad</div>
      <div style="margin-left: 6.5cm; margin-top: 0.4cm;">$contenido->fconfirmacion</div>
      <div style="margin-left: 4cm; margin-top: 0.4cm;">$contenido->cod_libro
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      $contenido->cod_foja
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      $contenido->cod_numero</div>
      <div style="margin-left: 7cm; margin-top: 0.4cm;">$contenido->cod_parroco</div>
      <div style="margin-left: 5cm; margin-top: 0.4cm;">$contenido->padrino_nombre</div>
      <div style="margin-left: 9cm; margin-top: 0.4cm;">$contenido->bautizoen</div>
      <div style="margin-left: 6cm; margin-top: 0.4cm;">$contenido->fbautizo</div>
      
    </div>    
  </body>
';
PDF;
