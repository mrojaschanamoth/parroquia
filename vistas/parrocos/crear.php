<form method="post" action="?c=parroco&a=crear" id="formCrear" autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Registro de Parroco Nuevo</h5>
  </div>

  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="Carnet">Carnet:</label>
          <input type="text" name="Carnet" placeholer="Ingrese carnet" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-6 hidden">
        <div class="form-group">
          <label for="IdParroco">Codigo de Parroco:</label>
          <input type="text" name="IdParroco" class="form-control" value="">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="Nombre">Nombre completo:</label>
          <input type="text" name="Nombre" placeholder="Ingrese nombres" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>
  </div>

  <div class="modal-footer">
    <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar</button>
  </div>
</form>