<form method="post" action="?c=parroco&a=editar" id="formEditar" autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Editar Registro</h5>
  </div>

  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="Nombre">Codigo:</label>
          <input type="text" name="IdParroco" class="form-control" readonly>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="Nombre">Nombre completo:</label>
          <input type="text" required name="Nombre" placeholder="Ingrese nombres" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="Carnet">Carnet:</label>
          <input type="text" name="Carnet" placeholder="Ingrese carnet" class="form-control" onKeyUp="this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>
  </div>

  <div class="modal-footer">
  <input type="hidden" name="IdParroco">
    <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar</button>
  </div>
</form>