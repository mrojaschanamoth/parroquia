<div class="table-responsive">
<table class="table table-hover table-responsive" id="tparroco">           
  <thead>
    <tr>
      <th>#</th>
      <th>Codigo</th>
      <th>Nombres</th>
      <th>Carnet</th>
      <th width="10%">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
      if ($respuesta->respuesta){
        $i = 1;
        $filas = $respuesta->resultado;

        foreach($filas as $fila): ?>
           <tr>
             <td><b><?php echo $i; $i++;?></b></td>
             <td><?php echo $fila->codigo; ?></td>
             <td><?php echo $fila->nombre; ?></td>
             <td><?php echo $fila->carnet; ?></td>
             <td>
                <a  <?php echo "href='?c=parroco&a=get&IdParroco=$fila->codigo'"; ?> class="btn  btn-sm  btn-warning editar"><i class="fa fa-edit"></i> </a>
                <a <?php echo "href='?c=parroco&a=eliminar&IdParroco=$fila->codigo'"; ?> class="btn  btn-sm  btn-danger  eliminar" ><i class="fa fa-trash"></i> </a>
             </td>
           </tr>          
        <?php  endforeach;
      }else{ ?>
          
          <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error!</strong><br>
            <span>Ocurrió un problema al cargar la tabla</span>
           
          </div>
          
     <?php }
    ?>
  </tbody>
</table>
</div>