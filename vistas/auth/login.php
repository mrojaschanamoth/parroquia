<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>.: Sistema Parroquia | Developer Technology :.</title>

  <!-- Favicons -->
  <link href="./recursos/dashio/img/icon.png" rel="icon">
  <link href="./recursos/dashio/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="./recursos/dashio/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="./recursos/dashio/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="./recursos/dashio/css/style.css" rel="stylesheet">
  <link href="./recursos/dashio/css/style-responsive.css" rel="stylesheet">
  
  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <div id="login-page">
    <div class="container">
      <form class="form-login" action="index.php" method="post" id="formLogin" autocomplete="off">
        <h2 class="form-login-heading">Login de Acceso</h2>
        <div class="login-wrap">
          <input type="text" class="form-control" placeholder="Usuario" name="Usuario" autofocus onKeyUp="this.value=this.value.toUpperCase();">
          <br>
          <input type="password" class="form-control" placeholder="Clave" name="Clave" onKeyUp="this.value=this.value.toUpperCase();">
          <br>
          <!--<label class="checkbox">
            <input type="checkbox" value="remember-me"> Remember me
            <span class="pull-right">
            <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
            </span>
            </label>-->
          <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> Acceder</button>
          <!--<hr>
          <div class="registration">
            Don't have an account yet?<br/>
            <a class="" href="#">
              Create an account
              </a>
          </div>-->
          <hr>
            <div class="login-social-link centered" align="center">
                      &copy; 2019 CopyRight. Todos los derechos reservados. <a href="https://developer-technology.com" target="_blank">Developer Technology.</a>
            </div>
        </div>
        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Forgot Password ?</h4>
              </div>
              <div class="modal-body">
                <p>Enter your e-mail address below to reset your password.</p>
                <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
              </div>
              <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                <button class="btn btn-theme" type="button">Submit</button>
              </div>
            </div>
            
          </div>
        </div>
        <!-- modal -->
      </form>
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="./recursos/dashio/lib/jquery/jquery.min.js"></script>
  <script src="./recursos/dashio/lib/bootstrap/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="./recursos/dashio/lib/jquery.backstretch.min.js"></script>
  <script type="text/javascript" src="./recursos/js/login.js"></script>
  <script>
    $.backstretch("./recursos/dashio/img/login-bg1.jpg", {
      speed: 500
    });
  </script>
</body>

</html>
