<div class="table-responsive">
<table class="table table-hover table-responsive" id="tmatrimonio">           
  <thead>
    <tr>
      <th>#</th>
      <th>Libro</th>
      <th>Foja</th>
      <th>Numero</th>
      <th>Novio</th>
      <th>Novia</th>
      <th width="15%">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
      if ($respuesta->respuesta){
        $i = 1;
        $filas = $respuesta->resultado->objetos;
        foreach($filas as $fila): ?>
           <tr>
             <td><b><?php echo $i; $i++;?></b></td>
             <td><?php echo $fila->cod_libro; ?></td>
             <td><?php echo $fila->cod_foja; ?></td>
             <td><?php echo $fila->cod_numero; ?></td>
             <td><?php echo $fila->novio_nombre.' '.$fila->novio_apPaterno.' '.$fila->novio_apMaterno; ?></td> 
             <td><?php echo $fila->novia_nombre.' '.$fila->novia_apPaterno.' '.$fila->novia_apMaterno; ?></td> 
             <td>
                <a href="?c=matrimonio&a=generar_constancia&IdMatrimonio=<?php echo $fila->codigo ?>" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-file-text"></i></a>
                <a href="?c=matrimonio&a=get&IdMatrimonio=<?php echo $fila->codigo ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                <a <?php echo "href='?c=matrimonio&a=eliminar&IdMatrimonio=$fila->codigo'"; ?>  class="btn btn-sm btn-danger eliminar"><i class="fa fa-trash"></i> </a>
             </td>
           </tr>          
        <?php  endforeach;
      }else{ ?>
          
          <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error!</strong><br>
            <span>Ocurrió un problema al cargar la tabla</span>
           
          </div>
          
     <?php }
    ?>
  </tbody>
</table>
</div>