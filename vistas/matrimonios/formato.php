<?php

$constancia = <<<PDF
  
  <body>
    
    <div>
      <div><p style="opacity: 0.7;">&nbsp;&nbsp;&nbsp;</p></div>
      <div style="margin-left: 6cm; margin-top: 1.5cm;">$contenido->cod_parroquia</div>
      <div style="margin-left: 10.5cm; margin-top: 0.4cm;">$contenido->cod_libro
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      $contenido->cod_foja
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      $contenido->cod_numero</div>
      <div style="margin-left: 5.5cm; margin-top: 0.4cm;">$contenido->novio_nombre $contenido->novio_apPaterno $contenido->novio_apMaterno</div>
      <div style="margin-left: 7cm; margin-top: 0.4cm;">$contenido->novio_nacimiento | $contenido->novio_fnacimiento</div>
      <div style="margin-left: 3.5cm; margin-top: 0.4cm;">$contenido->novio_padre_nombre $contenido->novio_padre_apPaterno $contenido->novio_padre_apMaterno</div>
      <div style="margin-left: 3.5cm; margin-top: 0.4cm;">$contenido->novio_madre_nombre $contenido->novio_madre_apPaterno $contenido->novio_madre_apMaterno</div>
      <div style="margin-left: 8cm; margin-top: 0.4cm;">$contenido->novio_parroquiabautismo</div>
      <div style="margin-left: 8cm; margin-top: 0.4cm;">$contenido->novio_fbautismo</div>
      <div style="margin-left: 5.5cm; margin-top: 0.4cm;">$contenido->novia_nombre $contenido->novia_apPaterno $contenido->novia_apMaterno</div>
      <div style="margin-left: 7cm; margin-top: 0.4cm;">$contenido->novia_nacimiento | $contenido->novia_fnacimiento</div>
      <div style="margin-left: 3.5cm; margin-top: 0.4cm;">$contenido->novia_padre_nombre $contenido->novia_padre_apPaterno $contenido->novia_padre_apMaterno</div>
      <div style="margin-left: 3.5cm; margin-top: 0.4cm;">$contenido->novia_madre_nombre $contenido->novia_madre_apPaterno $contenido->novia_madre_apMaterno</div>
      <div style="margin-left: 8cm; margin-top: 0.4cm;">$contenido->novia_parroquiabautismo</div>
      <div style="margin-left: 8cm; margin-top: 0.4cm;">$contenido->novia_fbautismo</div>
      <div style="margin-left: 6cm; margin-top: 0.4cm;">$contenido->fmatrimonio</div>
      <div style="margin-left: 5.5cm; margin-top: 0.4cm;">$contenido->cod_parroco</div>
      <div style="margin-left: 4cm; margin-top: 0.4cm;">$contenido->padrino | $contenido->madrina</div>
      <div style="margin-left: 4cm; margin-top: 0.4cm;">$contenido->testigo1 | $contenido->testigo2</div>
      <div style="margin-left: 6cm; margin-top: 0.4cm;">$contenido->anotaciones</div>
      
    </div>    
  </body>
';
PDF;
