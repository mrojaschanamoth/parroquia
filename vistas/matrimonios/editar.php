<section class="wrapper site-min-height">
  <h3><i class="fa fa-angle-right"></i>Actualizar Matrimonio</h3>
  <div class="content-panel" style="padding:20px;">
    <form method="post" action="?c=matrimonio&a=editar" >
        <div class="row">
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdLibro">Codigo de Libro:</label>
              <input type="text" name="IdLibro" class="form-control" value=<?php echo $contenido->cod_libro?>>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdFoja">Código de Foja:</label>
              <input type="text" name="IdFoja" class="form-control" required value=<?php echo $contenido->cod_foja?>>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdNumero">Código de Numero:</label>
              <input type="text" name="IdNumero" class="form-control" value=<?php echo $contenido->cod_numero?>>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdParroquia">Código de Parroquia:</label>
              <input type="text" name="IdParroquia" class="form-control" required value=<?php echo $contenido->cod_parroquia?>>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdParroco ">Codigo de Parroco:</label>
              <input type="text" name="IdParroco" class="form-control" value=<?php echo $contenido->cod_parroco?>>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="IdPadre ">Codigo de Padre:</label>
              <input type="text" name="IdPadre" class="form-control" value=<?php echo $contenido->cod_padre?>>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="FechaMatrimonio">Fecha de Matrimonio:</label>
              <input type="date" name="FechaMatrimonio" class="form-control" value=<?php echo $contenido->fmatrimonio?>>
            </div>
          </div>
        </div>
        <hr>
        <h5>Informacion del Novio</h5>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="NombreNovio">Nombre del Novio:</label>
              <input type="text" name="NombreNovio" class="form-control" value=<?php echo $contenido->novio_nombre?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="ApellidoPaternoNovio">Apellido paterno del Novio:</label>
              <input type="text" name="ApellidoPaternoNovio" class="form-control" value=<?php echo $contenido->novio_apPaterno?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="ApellidoMaternoNovio">Apellido materno del Novio:</label>
              <input type="text" name="ApellidoMaternoNovio" class="form-control"value=<?php echo $contenido->novio_apMaterno?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreNombreNovio">Nombre del padre del Novio:</label>
              <input type="text" name="PadreNombreNovio" class="form-control" value=<?php echo $contenido->novio_padre_nombre?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreApellidoPaternoNovio">Apellido Paterno del padre del novio:</label>
              <input type="text" name="PadreApellidoPaternoNovio" class="form-control" value=<?php echo $contenido->novio_padre_apPaterno?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreApellidoMaternoNovio">Apellido materno del padre del novio:</label>
              <input type="text" name="PadreApellidoMaternoNovio" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadreNombreNovio">Nombre de la madre del novio:</label>
              <input type="text" name="MadreNombreNovio" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadreApellidoPaternoNovio">Apellido Paterno de la madre del novio:</label>
              <input type="text" name="MadreApellidoPaternoNovio" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadreApellidoMaternoNovio">Apellido materno de la madre del novio:</label>
              <input type="text" name="MadreApellidoMaternoNovio" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="EstadoCivilNovio">Estado civil del Novio:</label>
              <input type="text" name="EstadoCivilNovio" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="LugarNacimientoNovio">Lugar de nacimiento del Novio:</label>
              <input type="text" name="LugarNacimientoNovio" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="FechaNacimientoNovio">Fecha de nacimiento del Novio:</label>
              <input type="date" name="FechaNacimientoNovio" class="form-control" value=<?php echo $contenido->novio_fnacimiento?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="EdadNovio">Edad del Novio:</label>
              <input type="text" name="EdadNovio" class="form-control" value=<?php echo $contenido->novio_edad?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="ParroquiaBautismoNovio">Parroquia de bautismo del novio:</label>
              <input type="text" name="ParroquiaBautismoNovio" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="FechaBautismoNovio">Fecha de Bautismo del novio:</label>
              <input type="date" name="FechaBautismoNovio" class="form-control" value=<?php echo $contenido->novio_fbautismo?>>
            </div>
          </div>
        </div>
         <hr>
         <h5>Informacion del Novia</h5>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="NombreNovia">Nombre del Novia:</label>
              <input type="text" name="NombreNovia" class="form-control" value=<?php echo $contenido->novia_nombre?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="ApellidoPaternoNovia">Apellido paterno del Novia:</label>
              <input type="text" name="ApellidoPaternoNovia" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="ApellidoMaternoNovia">Apellido materno del Novia:</label>
              <input type="text" name="ApellidoMaternoNovia" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreNombreNovia">Nombre del padre del Novia:</label>
              <input type="text" name="PadreNombreNovia" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreApellidoPaternoNovia">Apellido Paterno del padre del Novia:</label>
              <input type="text" name="PadreApellidoPaternoNovia" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="PadreApellidoMaternoNovia">Apellido materno del padre del Novia:</label>
              <input type="text" name="PadreApellidoMaternoNovia" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadreNombreNovia">Nombre de la madre del Novia:</label>
              <input type="text" name="MadreNombreNovia" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadreApellidoPaternoNovia">Apellido Paterno de la madre del Novia:</label>
              <input type="text" name="MadreApellidoPaternoNovia" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="MadreApellidoMaternoNovia">Apellido materno de la madre del Novia:</label>
              <input type="text" name="MadreApellidoMaternoNovia" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="EstadoCivilNovia">Estado civil del Novia:</label>
              <input type="text" name="EstadoCivilNovia" class="form-control" value=<?php echo $contenido->novia_ecivil?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="LugarNacimientoNovia">Lugar de nacimiento del Novia:</label>
              <input type="text" name="LugarNacimientoNovia" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="FechaNacimientoNovia">Fecha de nacimiento del Novia:</label>
              <input type="date" name="FechaNacimientoNovia" class="form-control" value=<?php echo $contenido->novia_fnacimiento?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="EdadNovia">Edad del Novia:</label>
              <input type="text" name="EdadNovia" class="form-control" value=<?php echo $contenido->novia_edad?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="ParroquiaBautismoNovia">Parroquia de bautismo del Novia:</label>
              <input type="text" name="ParroquiaBautismoNovia" class="form-control" value=<?php echo $contenido->novia_parroquiabautismo?>>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="FechaBautismoNovia">Fecha de Bautismo del Novia:</label>
              <input type="date" name="FechaBautismoNovia" class="form-control" value=<?php echo $contenido->novia_fbautismo?>>
            </div>
          </div>
        </div>
        <hr>
        <h5>Informacion Complementaria</h5>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="Padrino">Nombre del padrino:</label>
              <input type="text" name="Padrino" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Madrina">Nombre de la madrina:</label>
              <input type="text" name="Madrina" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Testigo1">Nombre de la Testigo1:</label>
              <input type="text" name="Testigo1" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Testigo2">Nombre de la Testigo2:</label>
              <input type="text" name="Testigo2" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Anotaciones">Anotaciones:</label>
              <input type="text" name="Anotaciones" class="form-control" value=<?php echo $contenido->anotaciones?>>
            </div>
          </div>
          <input type="hidden" name="IdMatrimonio" value=<?php echo $contenido->codigo?>>
          <br><br>
          <div class="col-md-12">
              <button type="submit"  class="btn btn-theme"><i class="fa fa-floppy-o" aria-hidden="true"> </i> Guardar cambios</button>
          </div>
    </form>
  </div>
</section>