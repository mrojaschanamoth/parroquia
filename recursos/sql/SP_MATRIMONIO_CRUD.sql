CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_matrimonio_crud`(
   codigo int,
   cod_padre int,
   cod_libro int,
   cod_foja int,
   cod_numero int,
   cod_parroco int,
   cod_parroquia int,
   fmatrimonio date, 
   novio_nombre varchar(30),
   novio_apPaterno varchar(30),
   novio_apMaterno varchar(30),
   novio_ecivil varchar(20),
   novio_nacimiento varchar(100),
   novio_fnacimiento date,
   novio_edad int,
   novio_parroquiabautismo varchar(100),
   novio_fbautismo date,
   novio_madre_nombre varchar(30),
   novio_madre_apPaterno varchar(30),
   novio_madre_apMaterno varchar(30),
   novio_padre_nombre varchar(30),
   novio_padre_apPaterno varchar(30),
   novio_padre_apMaterno varchar(30),
   novia_nombre varchar(30),
   novia_apPaterno varchar(30),
   novia_apMaterno varchar(30),
   novia_ecivil varchar(20),
   novia_nacimiento varchar(100),
   novia_fnacimiento date,
   novia_edad int,
   novia_parroquiabautismo varchar(100),
   novia_fbautismo date,
   novia_madre_nombre varchar(30),
   novia_madre_apPaterno varchar(30),
   novia_madre_apMaterno varchar(30),
   novia_padre_nombre varchar(30),
   novia_padre_apPaterno varchar(30),
   novia_padre_apMaterno varchar(30),
   testigo1 varchar(100),
   testigo2 varchar(100),
   padrino varchar(100),
   madrina varchar(100),
   anotaciones text, 
   opcion int,
   pagina int
)
BEGIN
  declare registrosPagina int;
  declare limiteInferior int;

  -- crear
	if opcion=1 then
		INSERT INTO `parroquia`.`matrimonio` (
        IdPadre,IdLibro,IdFoja,IdNumero,IdParroco,IdParroquia,FechaMatrimonio,NombreNovio,ApellidoPaternoNovio,ApellidoMaternoNovio,EstadoCivilNovio,LugarNacimientoNovio,FechaNacimientoNovio,EdadNovio,ParroquiaBautismoNovio,FechaBautismoNovio,NombrePadreNovio,ApellidoPaternoPadreNovio,ApellidoMaternoPadreNovio,NombreMadreNovio,ApellidoPaternoMadreNovio,ApellidoMaternoMadreNovio,NombreNovia,ApellidoPaternoNovia,ApellidoMaternoNovia,EstadoCivilNovia,LugarNacimientoNovia,FechaNacimientoNovia,EdadNovia,ParroquiaBautismoNovia,FechaBautismoNovia,NombrePadreNovia,ApellidoPaternoPadreNovia,ApellidoMaternoPadreNovia,NombreMadreNovia,ApellidoPaternoMadreNovia,ApellidoMaternoMadreNovia,Testigo1,Testigo2,Padrino,Madrina,Anotaciones)  VALUES (cod_padre,cod_libro,cod_foja,cod_numero,cod_parroquia,cod_parroco,fmatrimonio,novio_nombre,novio_apPaterno,novio_apMaterno,novio_ecivil,novio_nacimiento,novio_fnacimiento,novio_edad,novio_parroquiabautismo,novio_fbautismo,novio_padre_nombre,novio_padre_apPaterno,novio_padre_apMaterno,novio_madre_nombre,novio_madre_apPaterno,novio_madre_apMaterno,novia_nombre,novia_apPaterno,novia_apMaterno,novia_ecivil,novia_nacimiento,novia_fnacimiento,novia_edad,novia_parroquiabautismo,novia_fbautismo,novia_padre_nombre,novia_padre_apPaterno,novia_padre_apMaterno,novia_madre_nombre,novia_madre_apPaterno,novia_madre_apMaterno,testigo1,testigo2,padrino,madrina,anotaciones);
   end if;

  -- editar
  if opcion=2 then
    update matrimonio set
	  IdPadre = cod_padre,
      IdLibro = cod_libro,
      IdFoja = cod_foja,
      IdNumero = cod_numero,
      IdParroco = cod_parroco,
      IdParroquia = cod_parroquia,
      FechaMatrimonio = fmatrimonio, 
      NombreNovio = novio_nombre,
      ApellidoPaternoNovio = novio_apPaterno,
      ApellidoMaternoNovio = novio_apMaterno,
      EstadoCivilNovio = novio_ecivil,
      LugarNacimientoNovio = novio_nacimiento,
      FechaNacimientoNovio = novio_fnacimiento,
      EdadNovio = novio_edad,
      ParroquiaBautismoNovio = novio_parroquiabautismo,
      FechaBautismoNovio = novio_fbautismo,
      NombreMadreNovio = novio_madre_nombre,
      ApellidoMaternoMadreNovio= novio_madre_apPaterno,
      ApellidoMaternoMadreNovio= novio_madre_apMaterno,
      NombrePadreNovio=novio_padre_nombre,
      ApellidoPaternoPadreNovio=novio_padre_apPaterno,
      ApellidoMaternoPadreNovio=novio_padre_apMaterno, 
      NombreNovia=novia_nombre,
      ApellidoPaternoNovia=novia_apPaterno,
      ApellidoMaternoNovia=novia_apMaterno,
      EstadoCivilNovia=novia_ecivil,
      LugarNacimientoNovia=novia_nacimiento,
      FechaNacimientoNovia=novia_fnacimiento,
      EdadNovia=novia_edad,
      ParroquiaBautismoNovia=novia_parroquiabautismo,
      FechaBautismoNovia=novia_fbautismo,
      NombreMadreNovia=novia_madre_nombre,
      ApellidoPaternoMadreNovia=novia_madre_apPaterno,
      ApellidoMaternoMadreNovia=novia_madre_apMaterno,
      NombrePadreNovia=novia_padre_nombre,
      ApellidoPaternoPadreNovia=novia_padre_apPaterno,
      ApellidoMaternoPadreNovia=novia_padre_apMaterno,
      Testigo1=testigo1,
      Testigo2=testigo2,
      Padrino=padrino,
      Madrina=madrina,
      Anotaciones=anotaciones
    where IdMatrimonio = codigo;
  end if;
  
  -- eliminar
	if opcion=3 then
		delete from matrimonio where IdMatrimonio = codigo;
  end if;

	-- listar y buscar
	if opcion=4 then    
		set registrosPagina=10;
		set limiteInferior=(pagina-1)*registrosPagina;       

		select * from matrimonio where
      (NombreNovio like concat('%',novio_nombre,'%') or novio_nombre is null )
    limit limiteInferior,registrosPagina;
            
  -- numero de paginas
    select 
       case
				when mod(count(IdMatrimonio),registrosPagina)>0 then floor(count(IdMatrimonio) / registrosPagina) +1
          else floor(count(IdMatrimonio) / registrosPagina)
            end as paginas
            from matrimonio where 
            (NombreNovio like concat('%',novio_nombre,'%') or novio_nombre is null );
        end if;
  -- get
	if opcion=5 then
		select * from matrimonio where IdMatrimonio = codigo;
  end if;
END