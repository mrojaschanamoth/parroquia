CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_usuario_crud`(
  codigo int,
  usuario varchar(50),
  contra varchar(50),
  nombres varchar(50),
  apPaterno varchar(30),
  apMaterno varchar(30),
  direccion varchar(50),
  opcion int,
  pagina int
)
BEGIN
  declare registrosPagina int;
  declare limiteInferior int;

  -- crear
	if opcion=1 then
		insert usuario(Usuario,Clave,Nombre,ApellidoPaterno,ApellidoMaterno,Direccion) 
		values (usuario,contra,nombres,apPaterno,apMaterno,Direccion);
   end if;

  -- editar
  if opcion=2 then
    update usuario set
      Nombre = nombres,
      ApellidoPaterno = apPaterno,
      ApellidoMaterno = apMaterno,
      Usuario = usuario,
      Clave = clave,
      Direccion = direccion
    where IdUsuario = codigo;
  end if;
  
  -- eliminar
	if opcion=3 then
		delete from usuario where IdUsuario = codigo;
  end if;

	-- listar y buscar
	if opcion=4 then    
		set registrosPagina=10;
		set limiteInferior=(pagina-1)*registrosPagina;       

		select * from usuario where
      (Nombre like concat('%',nombres,'%') or nombres is null ) and 
      (ApellidoPaterno like concat('%',apPaterno,'%') or apPaterno is null) and
      (ApellidoMaterno like concat('%',apMaterno,'%') or apMaterno is null)
    limit limiteInferior,registrosPagina;
            
  -- numero de paginas
    select 
       case
				when mod(count(IdUsuario),registrosPagina)>0 then floor(count(IdUsuario) / registrosPagina) +1
          else floor(count(IdUsuario) / registrosPagina)
            end as paginas
            from usuario where 
            (Nombre like concat('%',nombres,'%') or nombres is null ) and 
            (ApellidoPaterno like concat('%',apPaterno,'%') or apPaterno is null) and
            (ApellidoMaterno like concat('%',apMaterno,'%') or apMaterno is null);
        end if;
  -- get
	if opcion=5 then
		select * from usuario where IdUsuario = codigo;
  end if;
  -- Consulta login
  if opcion = 6 then
    select * from usuario u where u.Usuario=usuario and u.Clave = contra;
  end if;
END