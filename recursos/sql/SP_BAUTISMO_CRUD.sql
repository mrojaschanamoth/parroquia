CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_bautismo_crud`(
  codigo int,
  cod_libro int,
  cod_foja int,
  cod_numero int,
  cod_parroquia varchar(02),
  padre_nombre varchar(255),
  padre_apPaterno varchar(255),
  padre_apMaterno varchar(255),
  nombre varchar(255),
  madre_nombre varchar(255),
  madre_apPaterno varchar(255),
  madre_apMaterno varchar(255),
  nacimiento varchar(255),
  fnacimiento date,
  fbautismo date,
  madrina_nombre varchar(255),
  madrina_apPaterno varchar(255),
  madrina_apMaterno varchar(255),
  padrino_nombre varchar(255),
  padrino_apPaterno varchar(255),
  padrino_apMaterno varchar(255),
  anotaciones text,
  sexo char(1),
  naturalmadre varchar(50),
  naturalpadre varchar(50),
  cod_distrito int,
  telefono varchar(15),
  opcion int,
  pagina int
)
BEGIN
  declare registrosPagina int;
  declare limiteInferior int;

  -- crear
	if opcion=1 then
		insert bautismo(IdLibro,IdFoja,IdNumero,IdParroquia,Nombre,PadreNombre,PadreApellidoPaterno,PadreApellidoMaterno,MadreNombre,MadreApellidoPaterno,MadreApellidoMaterno,LugarNacimiento,FechaNacimiento,FechaBautismo,MadrinaNombre,MadrinaApellidoPaterno,MadrinaApellidoMaterno,padrinoNombre,PadrinoApellidoPaterno,PadrinoApellidoMaterno,Anotaciones,Sexo,NaturalMadre,NaturalPadre,IdDistrito,Telefono)
		values (cod_libro,cod_foja,cod_numero,cod_parroquia,nombre,padre_nombre,padre_apPaterno,padre_apMaterno,madre_nombre,madre_apPaterno,madre_apMaterno,nacimiento,fnacimiento,fbautismo,madrina_nombre,madrina_apPaterno,madrina_apMaterno,padrino_nombre,padrino_apPaterno,padrino_apMaterno,anotaciones,sexo,naturalmadre,naturalpadre,cod_distrito,telefono);
   end if;

  -- editar
  if opcion=2 then
    update bautismo set
	  IdLibro = cod_libro,
      IdFoja = cod_foja,
      IdNumero = cod_numero,
      IdParroquia = cod_parroquia,
      Nombre = nombre,
      PadreNombre = padre_nombre,
      PadreApellidoPaterno = padre_apPaterno,
      PadreApellidoMaterno =padre_apMaterno,
      MadreNombre=madre_nombre,
      MadreApellidoPaterno = madre_apPaterno,
      MadreApellidoMaterno =madre_apMaterno,
      LugarNacimiento = nacimiento ,
      FechaNacimiento = fnacimiento,
      FechaBautismo = fbautismo,
      MadrinaNombre = madrina_nombre,
      MadrinaApellidoPaterno = madrina_apPaterno,
      MadrinaApellidoMaterno = madrina_apMaterno,
      padrinoNombre = padrino_nombre,
      PadrinoApellidoPaterno = padrino_apPaterno,
      PadrinoApellidoMaterno = padrino_apMaterno,
      Anotaciones = anotaciones,
      Sexo = sexo,
      NaturalMadre = naturalmadre,
      NaturalPadre = naturalpadre,
      IdDistrito = cod_distrito,
      Telefono = telefono
    where IdBautismo = codigo;
  end if;
  
  -- eliminar
	if opcion=3 then
		delete from bautismo where IdBautismo = codigo;
  end if;

	-- listar y buscar
	if opcion=4 then    
    select * from bautismo;
 end if;
  -- get
	if opcion=5 then
		select * from bautismo where IIdBautismo = codigo;
  end if;
END