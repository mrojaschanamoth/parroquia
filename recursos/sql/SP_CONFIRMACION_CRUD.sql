CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_confirmacion_crud`(
  codigo int,
  cod_libro int,
  cod_foja int,
  cod_numero int,
  cod_parroco int,
  cod_padre varchar(255),
  nombres varchar(255),
  apellidos varchar(255),
  padre_nombre varchar(255),
  madre_nombre varchar(255),
  fconfirmacion date,
  padrino_nombre varchar(255),
  confirmoen varchar(255),
  edad int,
  bautizoen varchar(255),
  fbautismo date,
  opcion int,
  pagina int
)
BEGIN
  declare registrosPagina int;
  declare limiteInferior int;

  -- crear
	if opcion=1 then
		insert confirmacion(Libro,Foja,Numero,IdParroco,IdPadre,Nombre,Apellidos,Padrenombre,MadreNombre,FechaConfirmacion,PadrinoNombre,ConfirmoEn,Edad,BautizadoEn,FechaBautizo)
		values (cod_libro,cod_foja,cod_numero,cod_parroco,cod_padre,nombres,apellidos,padre_nombre,madre_nombre,fconfirmacion,padrino_nombre,confirmoen,edad,bautizoen,fbautismo);
   end if;

  -- editar
  if opcion=2 then
    update confirmacion set
	  Libro = cod_libro,
      Foja = cod_foja,
      Numero = cod_numero,
      IdParroco = cod_parroco,
      IdPadre = cod_padre,
      Nombre = nombres,
      Apellidos = apellidos,
      PadreNombre = padre_nombre,
      MadreNombre = madre_nombre,
      FechaConfirmacion = fconfirmacion,
      PadrinoNombre = padrino_nombre,
      ConfirmoEn = confirmoen,
      Edad = edad,
      BautizadoEn = bautizoen,
      FechaBautizo = fbautismo
    where IdConfirmacion = codigo;
  end if;
  
  -- eliminar
	if opcion=3 then
		delete from confirmacion where IdConfirmacion = codigo;
  end if;

	-- listar y buscar
	if opcion=4 then    
      select * from confirmacion;
   end if;
  -- get
	if opcion=5 then
		select * from confirmacion where IdConfirmacion = codigo;
  end if;
END